#coding:utf8
import glob
import ctypes
import sys
import os
import shutil

kSummaryFileName = "summary.txt"
kSliceSec = 60

pcapFiles = glob.glob("*.pcap")
if len(pcapFiles) != 1:
    ctypes.windll.user32.MessageBoxA(
            0,
            "There are multiple pcap files or can't find a pcap file",
            "Error",
            0x40)
    sys.exit(-1)

if os.path.isdir("tmp"):
    shutil.rmtree("tmp")

os.mkdir("tmp")
os.system("editcap -i %d %s %s" % (
    kSliceSec,pcapFiles[0],"tmp/sliced.pcap"))
w = open("tmp\\%s" % kSummaryFileName,"w")
w.write("SliceSec,%d"% kSliceSec)
w.close()
