#coding:utf8
import glob
import os

pcapList = glob.glob("tmp/*.pcap")
loopCount = 0

sumTcpPortDict = dict()
sumUdpPortDict = dict()

for pcapFile in pcapList:
    tcpPortDict = dict()
    udpPortDict = dict()
    tcpPorts = os.popen("tshark -nn -r %s -T fields -e tcp.dstport (tcp)" %
            (pcapFile)).read().strip().split("\n")
    #一度も通信をしていない場合はpassする
    if len(tcpPorts) == 1 and tcpPorts[0] == "":
            pass
    else:
        for port in tcpPorts:
            tcpPortDict.setdefault(int(port),0)
            tcpPortDict[int(port)] += 1

    udpPorts = os.popen("tshark -nn -r %s -T fields -e udp.dstport (udp)" %
            (pcapFile)).read().strip().split("\n")
    if len(udpPorts) == 1 and udpPorts[0] == "":
            pass
    else:
        for port in udpPorts:
            udpPortDict.setdefault(int(port),0)
            udpPortDict[int(port)] += 1

    w = open("tmp\\%d.tcp.txt" % loopCount,"w")
    for key,value in tcpPortDict.iteritems():
        sumTcpPortDict.setdefault(key,0)
        sumTcpPortDict[key] += value
        w.write("%d,%d\n" % (key,value))
    w.close()

    w = open("tmp\\%d.udp.txt" % loopCount,"w")
    for key,value in udpPortDict.iteritems():
        sumUdpPortDict.setdefault(key,0)
        sumUdpPortDict[key] += value
        w.write("%d,%d\n" % (key,value))
    w.close()
    loopCount += 1


w = open("tmp\\tcp.txt","w")
for key,value in sumTcpPortDict.iteritems():
    w.write("%d,%d\n" % (key,value))
w.close()

w = open("tmp\\udp.txt","w")
for key,value in sumUdpPortDict.iteritems():
    w.write("%d,%d\n" % (key,value))
w.close()
