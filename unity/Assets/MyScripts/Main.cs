using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.IO;

public class Main : MonoBehaviour {

	// Use this for initialization
	private Texture2D texture;
	private float timer = 0.0f;
	private Dictionary<int,int> sumTcpPorts = new Dictionary<int, int>();
	private Dictionary<int,int> sumUcpPorts = new Dictionary<int, int>();
	
	void Start () {
		texture = new Texture2D(600,400,TextureFormat.RGBA32,false);
		StreamReader streamReader = new StreamReader(Application.dataPath+"/../../tmp/tcp.txt");
		string fileContents = "";
		fileContents = streamReader.ReadToEnd();
		//fileContents.Split('\n');
		foreach(string line in fileContents.Split('\n'))
		{
			if(line != "")
			{
				sumTcpPorts.Add (int.Parse(line.Split (',')[0]),int.Parse(line.Split(',')[1]));
			}
		}
		
		List<int> keyList = new List<int>();
		foreach(int key in sumTcpPorts.Keys)
		{
			keyList.Add (key);
		}
		keyList.Sort();
		print (keyList);
		foreach(int key in keyList)
		{
		print (key);
		}
		
		drawColor();
		
	
	}
	void OnGUI()
	{
		if(texture != null) GameObject.Find("Plane").renderer.material.mainTexture = texture;
	}
	// Update is called once per frame
	void Update () {
		/*
		timer += Time.deltaTime;
		if(timer >= 1.0f)
		{
			timer = 0.0f;
			drawColor();
		}
		*/
	
	}
	void drawColor()
	{
	        Color myColor = new Color();
            //texture.SetPixel(20,20,Color.red);
            DrawLine(texture,20,20,300,300,Color.red);
            DrawLine(texture,300,300,300,20,Color.red);
        	texture.Apply();
	}
	void DrawLine(Texture2D a_Texture, int x1, int y1, int x2, int y2, Color a_Color)
{
 
	float b = x2 - x1;
	float h = y2 - y1;
	float l = Mathf.Abs( b );
	if (Mathf.Abs ( h ) > l) l = Mathf.Abs( h );
	int il = (int)l;
	float dx = b / (float)l;
	float dy = h / (float)l;
	for ( int i = 0; i <= il; i++ )
	{
		a_Texture.SetPixel(x1, y1, a_Color);
 
		x1 += (int)dx;
		y1 += (int)dy;
	}
}
}
